;; Copyright (c) 2006-2019 Carlos Ungil

(in-package :rmath.ffi)

;; http://cran.r-project.org/doc/manuals/R-exts.html#Mathematical-functions

#| 6.7.2 Mathematical functions

    Function: double gammafn (double x)
    Function: double lgammafn (double x)
    Function: double digamma (double x)
    Function: double trigamma (double x)
    Function: double tetragamma (double x)
    Function: double pentagamma (double x)
    Function: double psigamma (double x, double deriv)

The Gamma function, the natural logarithm of its absolute value and first four
derivatives and the n-th derivative of Psi, the digamma function, which is the
derivative of lgammafn. In other words, digamma(x) is the same as psigamma(x,0),
trigamma(x) == psigamma(x,1), etc. |#

(cffi:defcfun "gammafn" :double (x :double))

(cffi:defcfun "lgammafn" :double (x :double))

(cffi:defcfun "digamma" :double (x :double))

(cffi:defcfun "trigamma" :double (x :double))

(cffi:defcfun "tetragamma" :double (x :double))

(cffi:defcfun "pentagamma" :double (x :double))

(cffi:defcfun "psigamma" :double (x :double) (deriv :double))

;; also exported 
;; see https://svn.r-project.org/R/trunk/src/nmath/lgamma.c
;; and https://svn.r-project.org/R/trunk/src/nmath/polygamma.c
;; double lgammafn_sign(double x, int *sgn);
;; void dpsifn(double x, int n, int kode, int m, double *ans, int *inz, int *ierr);

#| Function: double beta (double a, double b)
   Function: double lbeta (double a, double b)

The (complete) Beta function and its natural logarithm. |#

(cffi:defcfun "beta" :double (a :double) (b :double)) 

(cffi:defcfun "lbeta" :double (a :double) (b :double))

#| Function: double choose (double n, double k)
   Function: double lchoose (double n, double k)

The number of combinations of k items chosen from from n and the natural
logarithm of its absolute value, generalized to arbitrary real n. k is rounded
to the nearest integer (with a warning if needed). |#

(cffi:defcfun "choose" :double (a :double) (b :double))

(cffi:defcfun "lchoose" :double (a :double) (b :double))

#| Function: double bessel_i (double x, double nu, double expo)
   Function: double bessel_j (double x, double nu)
   Function: double bessel_k (double x, double nu, double expo)
   Function: double bessel_y (double x, double nu)

Bessel functions of types I, J, K and Y with index nu. For bessel_i and bessel_k
there is the option to return exp(-x) I(x; nu) or exp(x) K(x; nu) if expo is
2. (Use expo == 1 for unscaled values.) |#

(cffi:defcfun "bessel_i" :double (x :double) (nu :double) (expo :double))

(cffi:defcfun "bessel_j" :double (x :double) (nu :double))

(cffi:defcfun "bessel_k" :double (x :double) (nu :double) (expo :double))

(cffi:defcfun "bessel_y" :double (x :double) (nu :double))

;; also exported - accepting a work array instead of allocating one
;; see https://svn.r-project.org/R/trunk/src/nmath/bessel_i.c etc.
;; double bessel_i_ex(double x, double alpha, double expo, double *bi);
;; double bessel_j_ex(double x, double alpha, double *bj);
;; double bessel_k_ex(double x, double alpha, double expo, double *bk);
;; double bessel_y_ex(double x, double alpha, double *by);
