;; Copyright (c) 2006-2019 Carlos Ungil

(in-package #:asdf)

(defsystem #:rmath
    :name "RMATH"
    :author "Carlos Ungil <ungil@mac.com>"
    :license "MIT"
    :description "Wrapper for the standalone Rmath library.
https://cran.r-project.org/doc/manuals/R-admin.html#The-standalone-Rmath-library"
    :depends-on (:cffi)
    :in-order-to ((test-op (test-op "rmath/test")))
    :components ((:file "package")
                 (:file "init" :depends-on ("package"))
		 (:file "random" :depends-on ("init"))
		 (:file "distributions" :depends-on ("init"))
		 (:file "functions" :depends-on ("init"))
		 (:file "internals" :depends-on ("init"))
                 (:file "utilities" :depends-on ("init"))
		 (:file "constants" :depends-on ("init"))
		 (:file "wrappers" :depends-on ("random" "distributions"
						"functions" "internals"))))

(defsystem #:rmath/test
    :name "RMATHTEST"
    :depends-on (:rmath :fiveam)
    :components ((:file "test"))
    :perform (asdf:test-op (o s) (uiop:symbol-call :rmath.test '#:run)))
