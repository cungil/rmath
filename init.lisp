;; Copyright (c) 2006-2019 Carlos Ungil

(in-package :rmath)

(cffi:define-foreign-library rmath
  (:windows #.(merge-pathnames #+CFFI-FEATURES:X86-64 "lib/Rmath64.dll" ;; 3.5.2
			       #-CFFI-FEATURES:X86-64 "lib/Rmath32.dll" ;; 3.5.2
			       (asdf:system-source-directory :rmath)))
  (:darwin #.(merge-pathnames #+CFFI-FEATURES:X86-64 "lib/libRmath64.dylib" ;; 3.5.2
			      #-CFFI-FEATURES:X86-64 "lib/libRmath32.dylib" ;; 2.15.3
			      (asdf:system-source-directory :rmath)))
  (t (:default "libRmath")))

(cffi:load-foreign-library 'rmath)
