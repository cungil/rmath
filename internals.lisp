;; Copyright (c) 2006-2019 Carlos Ungil

(in-package :rmath.ffi)

;; https://cran.r-project.org/doc/manuals/R-admin.html#The-standalone-Rmath-library

#| 9 The standalone Rmath library

The routines supporting the distribution and special36 functions in R and a few
others are declared in C header file Rmath.h. These can be compiled into a
standalone library for linking to other applications. (Note that they are not a
separate library when R is built, and the standalone version differs in several
ways.)

The makefiles and other sources needed are in directory src/nmath/standalone, so
the following instructions assume that is the current working directory (in the
build directory tree on a Unix-alike if that is separate from the sources).

Rmath.h contains ‘R_VERSION_STRING’, which is a character string containing the
current R version, for example "3.5.0".

There is full access to R’s handling of NaN, Inf and -Inf via special versions
of the macros and functions

    ISNAN, R_FINITE, R_log, R_pow and R_pow_di

and (extern) constants R_PosInf, R_NegInf and NA_REAL.

There is no support for R’s notion of missing values, in particular not for
NA_INTEGER nor the distinction between NA and NaN for doubles.

A little care is needed to use the random-number routines. You will need to
supply the uniform random number generator

    double unif_rand(void)

or use the one supplied (and with a shared library or DLL you may have to use
the one supplied, which is the Marsaglia-multicarry with an entry point

    set_seed(unsigned int, unsigned int)

to set its seeds).

The facilities to change the normal random number generator are available
through the constant N01_kind. This takes values from the enumeration type

typedef enum {
    BUGGY_KINDERMAN_RAMAGE,
    AHRENS_DIETER,
    BOX_MULLER,
    USER_NORM,
    INVERSION,
    KINDERMAN_RAMAGE
} N01type;

(and ‘USER_NORM’ is not available). |#

(cffi:defcfun "R_isnancpp" :int (x :double))

(cffi:defcfun "R_finite":int (x :double))

(cffi:defcvar "NA_REAL" :double)

(cffi:defcvar "R_PosInf" :double)

(cffi:defcvar "R_NegInf" :double)

(cffi:defcvar "N01_kind" :int)
