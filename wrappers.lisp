;; Copyright (c) 2006-2019 Carlos Ungil

(in-package :rmath)

;; Distributions

(defmacro defdist (name parameters documentation &key free (output '(r d p q)))
  `(progn 
     ,(if (member 'r output) 
	  `(defun ,(intern (concatenate 'string "R-" (symbol-name name)))
	     ,parameters
	     ,documentation
	     ,(append (list (intern (concatenate 'string "R" (symbol-name name)) "RMATH.FFI"))
		      (loop for parameter in parameters collect `(coerce ,parameter 'double-float)))))
     ,(if (member 'd output) 
	  `(defun ,(intern (concatenate 'string "D-" (symbol-name name)))
	     ,(append (cons 'x parameters) '(&key give-log))
	     ,documentation
	     (prog1
		 ,(append (list (intern (concatenate 'string "D" (symbol-name name)) "RMATH.FFI"))
			  '((coerce x 'double-float))
			  (loop for parameter in parameters collect `(coerce ,parameter 'double-float))
			  '((if give-log 1 0)))
	       ,(when free (list (intern (symbol-name free) "RMATH.FFI"))))))
     ,(if (member 'p output) 
	  `(defun ,(intern (concatenate 'string "P-" (symbol-name name)))
	     ,(append (cons 'x parameters) '(&key upper-tail give-log))
	     ,documentation
	     (prog1 
		 ,(append (list (intern (concatenate 'string "P" (symbol-name name)) "RMATH.FFI"))
			  '((coerce x 'double-float))
			  (loop for parameter in parameters collect `(coerce ,parameter 'double-float))
			  '((if upper-tail 1 0) (if give-log 1 0)))
	       ,(when free (list (intern (symbol-name free) "RMATH.FFI"))))))
     ,(if (member 'q output) 
	  `(defun ,(intern (concatenate 'string "Q-" (symbol-name name)))
	     ,(append (cons 'x parameters) '(&key upper-tail log-p))
	     ,documentation
	     (prog1 
		 ,(append (list (intern (concatenate 'string "Q" (symbol-name name)) "RMATH.FFI"))
			  '((coerce x 'double-float))
			  (loop for parameter in parameters collect `(coerce ,parameter 'double-float))
			  '((if upper-tail 1 0) (if log-p 1 0)))
	       ,(when free (list (intern (symbol-name free) "RMATH.FFI"))))))))
  
(defdist norm (mu sigma) "Normal distribution")

(defdist unif (a b) "Uniform distribution")

(defdist gamma (shape scale) "Gamma distribution")

(defdist beta (a b) "Beta distribution")

(defdist lnorm (logmean logsigma) "Lognormal distribution")
    
(defdist chisq (df) "Chi-Squared distribution")

(defdist nchisq (df lambda) "non-central Chi-Squared distribution")

(defdist f (n1 n2) "F distribution")

(defdist t (n) "Student t distribution")

(defdist binom (n p) "Binomial distribution")

(defdist nbinom (n p) "Negative Binomial distribution")

(defdist cauchy (location scale) "Chauchy distribution")

(defdist exp (scale) "Exponential distribution")

(defdist geom (p) "Geometric distribution")

(defdist hyper (NR NB n) "Hypergeometric distribution")

(defdist pois (lambda) "Poisson distribution")

(defdist weibull (shape scale) "Weibull distribution")

(defdist logis (location scale) "Logistic distribution")

(defdist wilcox (m n) "Wilcoxon Rank Sum distribution" :free wilcox-free)

(defdist signrank (n) "Wilcoxon Signed Rank distribution" :free signrank-free)

(defdist nbeta (a b lambda) "non-central Beta distribution" :output (d p q))

(defdist nf (n1 n2 ncp) "non-central F distribution" :output (d p q))

(defdist nt (df delta) "non-central Student t distribution" :output (d p q))

(defdist tukey (rr cc df) "Studentized Range distribution" :output (p q))

;; Functions

(defun gamma (x &optional return-log)
  "Gamma function (if return-log=T the logarithm is returned)"
  (if return-log
      (rmath.ffi::lgammafn (coerce x 'double-float))
  (rmath.ffi::gammafn (coerce x 'double-float))))

(defun psi (x &optional (derivative 0))
  "Psi (digamma) function, or a derivative of it"
  (unless (and (integerp derivative) 
	       (not (minusp derivative)))
    (error "the order of the derivative should be a non-negative integer"))
  (case derivative
    (0 (rmath.ffi::digamma (coerce x 'double-float)))
    (1 (rmath.ffi::trigamma (coerce x 'double-float)))
    (2 (rmath.ffi::tetragamma (coerce x 'double-float)))
    (3 (rmath.ffi::pentagamma (coerce x 'double-float)))
    (t (rmath.ffi::psigamma (coerce x 'double-float) 
			     (coerce derivative 'double-float)))))

(defun beta (a b &optional return-log)
  "Beta function (if return-log=T the logarithm is returned)"
  (if return-log
      (rmath.ffi::lbeta (coerce a 'double-float) (coerce b 'double-float))
    (rmath.ffi::beta (coerce a 'double-float) (coerce b 'double-float))))

(defun choose (n k &optional return-log)
  "number of combinations of k times chosen from n, (k and n rounded to the 
nearest integer); if return-log=T the logarithm is returned"
  (if return-log
      (rmath.ffi::lchoose (coerce n 'double-float) (coerce k 'double-float))
    (rmath.ffi::choose (coerce n 'double-float) (coerce k 'double-float))))

(defun bessel (type x nu &optional return-exp)
  "Bessel functions: type should be :j (:y) for the Bessel function of the first 
(second) kind and :i (:k) for the modified functions of first (second) kind; 
for the modified functions, return-exp=T results in exp(-x)I(x;nu) or exp(x)K(x; nu)"
  (unless (member type '(:j :y :i :k))
    (error "type should be one of :j (1st kind), :y (2nd kind), :i (modified 1st kind), or :k (modified 2nd kind)"))
  (when (and return-exp (member type '(:j :y)))
    (error "the return-exponential argument is only valid for modified functions"))
  (case type
    (:j (rmath.ffi::bessel-j (coerce x 'double-float) (coerce nu 'double-float)))
    (:y (rmath.ffi::bessel-y (coerce x 'double-float) (coerce nu 'double-float)))
    (:i (rmath.ffi::bessel-i (coerce x 'double-float) (coerce nu 'double-float)
			      (if return-exp 2d0 1d0)))
    (:k (rmath.ffi::bessel-k (coerce x 'double-float) (coerce nu 'double-float)
			      (if return-exp 2d0 1d0)))))
 
;; Random numbers

(defun random-normal ()
  "normally distributed pseudo-random number"
  (rmath.ffi::norm-rand))

(defun random-uniform ()
  "uniformly distributed in [0,1] pseudo-random number"
  (rmath.ffi::unif-rand))

(defun random-exponential ()
  "exponentially distributed pseudo-random number"
  (rmath.ffi::exp-rand))

(defun rng-get-seed ()
  (cffi:with-foreign-object 
      (seed1 :unsigned-int)
    (cffi:with-foreign-object 
	(seed2 :unsigned-int)
      (rmath.ffi::get-seed seed1 seed2)
      (list (cffi:mem-ref seed1 :unsigned-int) (cffi:mem-ref seed2 :unsigned-int)))))

(defun rng-set-seed (seed)
  (unless (and (integerp (first seed))
	       (integerp (second seed))
	       (null (third seed)))
    (error "the seed should be a list of two integers"))
  (rmath.ffi::set-seed (first seed) (second seed))
  (assert (equal (rng-get-seed) seed)))

(defun rng-seed ()
  "get (or set) the seed of the random number generator (a list of two integers)"
  (rng-get-seed))

(defsetf rng-seed rng-set-seed)

(defvar *normal-rng-kinds* '(:buggy-kinderman-ramage
			     :ahrens-dieter
			     :box-muller
			     :user-norm
			     :inversion
			     :kinderman-ramage))

(defun get-normal-rng-kind ()
  (elt *normal-rng-kinds* rmath.ffi::*n01-kind*))

(defun set-normal-rng-kind (kind)
  (assert (and (member kind *normal-rng-kinds*)
	       (not (eq kind :user-norm))))
  (setf rmath.ffi::*n01-kind* (position kind *normal-rng-kinds*))
  (assert (equal (get-normal-rng-kind) kind)))

(defun normal-rng-kind ()
  "get (or set) the kind of normal rng; one of :inversion (default)
:ahrens-dieter :box-muller :kinderman-ramage :buggy-kinderman-ramage"
  (get-normal-rng-kind))

(defsetf normal-rng-kind set-normal-rng-kind)

;; Special numbers

(defun isnan (x)
  (= 1 (rmath.ffi::r-isnancpp x)))

(defun isfinite (x)
  (= 1 (rmath.ffi::r-finite x)))

(defvar *na* rmath.ffi::*na-real*)

(defvar *posinf* rmath.ffi::*r-posinf*)

(defvar *neginf* rmath.ffi::*r-neginf*)

