;; Copyright (c) 2006-2019 Carlos Ungil

(in-package :rmath.ffi)

;; http://cran.r-project.org/doc/manuals/R-exts.html#Mathematical-constants

#| 6.7.4 Mathematical constants

R has a set of commonly used mathematical constants encompassing constants
defined by POSIX and usually143 found in math.h (but maybe not in the C++ header
cmath) and contains further ones that are used in statistical
computations. These are defined to (at least) 30 digits accuracy in Rmath.h. The
following definitions use ln(x) for the natural logarithm (log(x) in R).

  Name            Definition (ln = log) round(value, 7)
  M_E             e                     2.7182818
  M_LOG2E         log2(e)               1.4426950
  M_LOG10E        log10(e)              0.4342945
  M_LN2           ln(2)                 0.6931472
  M_LN10          ln(10)                2.3025851
  M_PI            pi                    3.1415927
  M_PI_2          pi/2                  1.5707963
  M_PI_4          pi/4                  0.7853982
  M_1_PI          1/pi                  0.3183099
  M_2_PI          2/pi                  0.6366198
  M_2_SQRTPI      2/sqrt(pi)            1.1283792
  M_SQRT2         sqrt(2)               1.4142136
  M_SQRT1_2       1/sqrt(2)             0.7071068
  M_SQRT_3        sqrt(3)               1.7320508
  M_SQRT_32       sqrt(32)              5.6568542
  M_LOG10_2       log10(2)              0.3010300
  M_2PI           2*pi                  6.2831853
  M_SQRT_PI       sqrt(pi)              1.7724539
  M_1_SQRT_2PI    1/sqrt(2*pi)          0.3989423
  M_SQRT_2dPI     sqrt(2/pi)            0.7978846
  M_LN_SQRT_PI    ln(sqrt(pi))          0.5723649
  M_LN_SQRT_2PI   ln(sqrt(2*pi))        0.9189385
  M_LN_SQRT_PId2  ln(sqrt(pi/2))        0.2257914

There are a set of constants (PI, DOUBLE_EPS) (and so on) defined (unless
STRICT_R_HEADERS is defined) in the included header R_ext/Constants.h, mainly
for compatibility with S.

Further, the included header R_ext/Boolean.h has enumeration constants TRUE and
FALSE of type Rboolean in order to provide a way of using “logical” variables in
C consistently. This can conflict with other software: for example it conflicts
with the headers in IJG’s jpeg-9 (but not earlier versions). |#


(cl:defparameter E 2.718281828459045235360287471353d0 "e")
(cl:defparameter LOG2E 1.442695040888963407359924681002d0 "log2(e)")
(cl:defparameter LOG10E 0.434294481903251827651128918917d0 "log10(e)")
(cl:defparameter LN2 0.693147180559945309417232121458d0 "ln(2)")
(cl:defparameter LN10 2.302585092994045684017991454684d0 "ln(10)")
(cl:defparameter PI 3.141592653589793238462643383280d0 "pi")
(cl:defparameter 2PI 6.283185307179586476925286766559d0 "2*pi")
(cl:defparameter PI-2 1.570796326794896619231321691640d0 "pi/2")
(cl:defparameter PI-4 0.785398163397448309615660845820d0 "pi/4")
(cl:defparameter 1-PI 0.318309886183790671537767526745d0 "1/pi")
(cl:defparameter 2-PI 0.636619772367581343075535053490d0 "2/pi")
(cl:defparameter 2-SQRTPI 1.128379167095512573896158903122d0 "2/sqrt(pi)")
(cl:defparameter SQRT2 1.414213562373095048801688724210d0 "sqrt(2)")
(cl:defparameter SQRT1-2 0.707106781186547524400844362105d0 "1/sqrt(2)")
(cl:defparameter SQRT-3 1.732050807568877293527446341506d0 "sqrt(3)")
(cl:defparameter SQRT-32 5.656854249492380195206754896838d0 "sqrt(32)")
(cl:defparameter LOG10-2 0.301029995663981195213738894724d0 "log10(2)")
(cl:defparameter SQRT-PI 1.772453850905516027298167483341d0 "sqrt(pi)")
(cl:defparameter 1-SQRT-2PI 0.398942280401432677939946059934d0 "1/sqrt(2pi)")
(cl:defparameter SQRT-2dPI 0.797884560802865355879892119869d0 "sqrt(2/pi)")
(cl:defparameter LN-2PI 1.837877066409345483560659472811d0 "log(2*pi)")
(cl:defparameter LN-SQRT-PI 0.572364942924700087071713675677d0 "log(sqrt(pi))")
(cl:defparameter LN-SQRT-2PI 0.918938533204672741780329736406d0 "log(sqrt(2*pi))")
(cl:defparameter LN-SQRT-PId2 0.225791352644727432363097614947d0 "log(sqrt(pi/2))")
