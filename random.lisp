;; Copyright (c) 2006-2019 Carlos Ungil

(in-package :rmath.ffi)

;; http://cran.r-project.org/doc/manuals/R-exts.html#Random-numbers
;; http://cran.r-project.org/doc/manuals/R-exts.html#Standalone-Mathlib

#| 6.3 Random number generation

The interface to R’s internal random number generation routines is

double unif_rand();
double norm_rand();
double exp_rand();

giving one uniform, normal or exponential pseudo-random variate. However, before
these are used, the user must call

GetRNGstate();

and after all the required variates have been generated, call

PutRNGstate();

These essentially read in (or create) .Random.seed and write it out after use.

File S.h defines seed_in and seed_out for S-compatibility rather than
GetRNGstate and PutRNGstate. These take a long * argument which is ignored.

The random number generator is private to R; there is no way to select the kind
of RNG or set the seed except by evaluating calls to the R functions.

The C code behind R’s rxxx functions can be accessed by including the header
file Rmath.h; See Distribution functions. Those calls generate a single variate
and should also be enclosed in calls to GetRNGstate and PutRNGstate. |#

(cffi:defcfun "norm_rand" :double)

(cffi:defcfun "unif_rand" :double)

(cffi:defcfun "exp_rand" :double)

#| 6.17 Using these functions in your own C code

It is possible to build Mathlib, the R set of mathematical functions documented
in Rmath.h, as a standalone library libRmath under both Unix-alikes and
Windows. (This includes the functions documented in Numerical analysis
subroutines as from that header file.)

The library is not built automatically when R is installed, but can be built in
the directory src/nmath/standalone in the R sources: see the file README
there. To use the code in your own C program include

#define MATHLIB_STANDALONE
#include <Rmath.h>

and link against ‘-lRmath’ (and perhaps ‘-lm’). There is an example file test.c.

A little care is needed to use the random-number routines. You will need to
supply the uniform random number generator

double unif_rand(void)

or use the one supplied (and with a dynamic library or DLL you will have to use
the one supplied, which is the Marsaglia-multicarry with an entry points

set_seed(unsigned int, unsigned int)

to set its seeds and

get_seed(unsigned int *, unsigned int *)

to read the seeds). |#

(cffi:defcfun "set_seed" :void (a :int) (b :int))

(cffi:defcfun "get_seed" :void (a (:pointer :unsigned-int)) (b (:pointer :unsigned-int)))
