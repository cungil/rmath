;; Copyright (c) 2006-2019 Carlos Ungil

(in-package :rmath.ffi)

;; https://cran.r-project.org/doc/manuals/R-exts.html#Numerical-Utilities

#| 6.7.3 Numerical Utilities

There are a few other numerical utility functions available as entry points.

    Function: double R_pow (double x, double y)
    Function: double R_pow_di (double x, int i)

R_pow(x, y) and R_pow_di(x, i) compute x^y and x^i, respectively using R_FINITE
checks and returning the proper result (the same as R) for the cases where x, y
or i are 0 or missing or infinite or NaN. |#

(cffi:defcfun ("R_pow" pow) :double (x :double) (y :double))

(cffi:defcfun ("R_pow_di" pow-di)  :double (x :double) (i :int))

#|  Function: double log1p (double x)

Computes log(1 + x) (log 1 plus x), accurately even for small x, i.e., |x| << 1.

This should be provided by your platform, in which case it is not included in
Rmath.h, but is (probably) in math.h which Rmath.h includes (except under C++,
so it may not be declared for C++98). |#

(cffi:defcfun "log1p" :double (x :double)) ;; = log(1+x) {care for small x} 

#|  Function: double log1pmx (double x)

Computes log(1 + x) - x (log 1 plus x minus x), accurately even for small x,
i.e., |x| << 1. |#

(cffi:defcfun "log1pmx" :double (x :double)) ;; = log(1+x) - x, {care for small x} 

#|  Function: double log1pexp (double x)

Computes log(1 + exp(x)) (log 1 plus exp), accurately, notably for large x,
e.g., x > 720. |#

;; double  log1pexp(double); // <-- ../nmath/plogis.c

(cffi:defcfun "log1pexp" :double (x :double)) ;; FIXME

#|  Function: double expm1 (double x)

Computes exp(x) - 1 (exp x minus 1), accurately even for small x, i.e., |x| <<
1.

This should be provided by your platform, in which case it is not included in
Rmath.h, but is (probably) in math.h which Rmath.h includes (except under C++,
so it may not be declared for C++98). |#

#| Function: double lgamma1p (double x)

Computes log(gamma(x + 1)) (log(gamma(1 plus x))), accurately even for small x,
i.e., 0 < x < 0.5. |#

(cffi:defcfun "lgamma1p" :double (x :double)) ;; = log(gamma(x+1)), small x (0 < x < 0.5)

#|  Function: double cospi (double x)

Computes cos(pi * x) (where pi is 3.14159...), accurately, notably for half
integer x.

This might be provided by your platform142, in which case it is not included in
Rmath.h, but is in math.h which Rmath.h includes. (Ensure that neither math.h
nor cmath is included before Rmath.h or define

    #define __STDC_WANT_IEC_60559_FUNCS_EXT__ 1

before the first inclusion.) |#

(cffi:defcfun "cospi" :double (x :double)) ;; FIXME

#|  Function: double sinpi (double x)

    Computes sin(pi * x) accurately, notably for (half) integer x.

This might be provided by your platform, in which case it is not included in
Rmath.h, but is in math.h which Rmath.h includes (but see the comments for
cospi). |#

(cffi:defcfun "sinpi" :double (x :double)) ;; FIXME

#|  Function: double tanpi (double x)

Computes tan(pi * x) accurately, notably for (half) integer x.

This might be provided by your platform, in which case it is not included in
Rmath.h, but is in math.h which Rmath.h includes (but see the comments for
cospi). |#

(cffi:defcfun "tanpi" :double (x :double)) ;; FIXME

#|  Function: double logspace_add (double logx, double logy)
    Function: double logspace_sub (double logx, double logy)
    Function: double logspace_sum (const double* logx, int n)

Compute the log of a sum or difference from logs of terms, i.e., “x + y” as
log (exp(logx) + exp(logy)) and “x - y” as log (exp(logx) - exp(logy)), and
“sum_i x[i]” as log (sum[i = 1:n exp(logx[i])] ) without causing unnecessary
overflows or throwing away too much accuracy. |#

(cffi:defcfun "logspace_add" :double (logx :double) (logy :double))

(cffi:defcfun "logspace_sub" :double (logx :double) (logy :double))

(cffi:defcfun "logspace_sum" :double (logx (:pointer :double)) (n :int))

#|  Function: int imax2 (int x, int y)
    Function: int imin2 (int x, int y)
    Function: double fmax2 (double x, double y)
    Function: double fmin2 (double x, double y)

Return the larger (max) or smaller (min) of two integer or double numbers,
respectively. Note that fmax2 and fmin2 differ from C99/C++11’s fmax and fmin
when one of the arguments is a NaN: these versions return NaN. |#

(cffi:defcfun "imax2" :int (x :int) (y :int))

(cffi:defcfun "imin2" :int (x :int) (y :int))

(cffi:defcfun "fmax2" :double (x :double) (y :double))

(cffi:defcfun "fmin2" :double (x :double) (y :double))

#|  Function: double sign (double x)

Compute the signum function, where sign(x) is 1, 0, or -1, when x is positive,
0, or negative, respectively, and NaN if x is a NaN. |#

(cffi:defcfun "sign" :double (x :double))

#|  Function: double fsign (double x, double y)

Performs “transfer of sign” and is defined as |x| * sign(y). |#

(cffi:defcfun "fsign" :double (x :double) (y :double))

#|  Function: double fprec (double x, double digits)

Returns the value of x rounded to digits decimal digits (after the decimal
point).

This is the function used by R’s signif(). |#

(cffi:defcfun "fprec" :double (x :double) (digits :double))

#|  Function: double fround (double x, double digits)

Returns the value of x rounded to digits significant decimal digits.

This is the function used by R’s round(). (Note that C99/C++11 provide a round
function but C++98 need not.) |#

(cffi:defcfun "fround" :double (x :double) (digits :double))

#|  Function: double ftrunc (double x)

Returns the value of x truncated (to an integer value) towards zero.

(Note that C99/C++11 provide a round function but C++98 need not.) |#

(cffi:defcfun "ftrunc" :double (x :double))
