;; Copyright (c) 2006-2019 Carlos Ungil

(in-package :cl-user)

(defpackage "RMATH"
  (:use "CL")
  (:export ;; isNaN - isFinite - NaN - negativeInf - positiveInf
           "ISNAN" "ISFINITE" "*NA*" "*NEGINF*" "*POSINF*"

	   ;; Random Uniform / Normal / Exponential
	   "RANDOM-UNIFORM" "RANDOM-NORMAL" "RANDOM-EXPONENTIAL"
	   ;; RNG seed and method for normal-number generation 
	   "RNG-SEED" "NORMAL-RNG-KIND"

	   ;; Bessel - Beta - Choose - Gamma and derivatives
	   "BESSEL" "BETA" "CHOOSE" "GAMMA" "PSI"

	   ;; Distributions
	   ;; r-... random values
	   ;; d-... density
	   ;; p-... cumulative distribution
	   ;; q-... quantile 
	   
	   ;; Uniform
	   "R-UNIF" "D-UNIF" "P-UNIF" "Q-UNIF"
	   ;; Normal
	   "R-NORM" "D-NORM" "P-NORM" "Q-NORM"
	   ;; Log-normal
	   "R-LNORM" "D-LNORM" "P-LNORM" "Q-LNORM"
	   ;; Gamma
	   "R-GAMMA" "D-GAMMA" "P-GAMMA" "Q-GAMMA"
	   ;; Beta
	   "R-BETA" "D-BETA" "P-BETA" "Q-BETA"
	   ;; Non-central beta
	   "D-NBETA" "P-NBETA" "Q-NBETA"
	   ;; Chi-squared
	   "R-CHISQ" "D-CHISQ" "P-CHISQ" "Q-CHISQ"
	   ;; Non-central Chi-squared
	   "R-NCHISQ" "D-NCHISQ" "P-NCHISQ" "Q-NCHISQ"
	   ;; Student's t
	   "R-T" "D-T" "P-T" "Q-T"
	   ;; Non-central t
	   "D-NT" "P-NT" "Q-NT"
	   ;; F
	   "R-F" "D-F" "P-F" "Q-F"
	   ;; Non-central F
	   "D-NF" "P-NF" "Q-NF"
	   ;; Binomial
	   "R-BINOM" "D-BINOM" "P-BINOM" "Q-BINOM"
	   ;; Negative binomial
	   "R-NBINOM" "D-NBINOM" "P-NBINOM" "Q-NBINOM"
	   ;; Exponential
	   "R-EXP" "D-EXP" "P-EXP" "Q-EXP"
	   ;; Cauchy
	   "R-CAUCHY" "D-CAUCHY" "P-CAUCHY" "Q-CAUCHY"
	   ;; Poisson
	   "R-POIS" "D-POIS" "P-POIS" "Q-POIS"
	   ;; Geometric
	   "R-GEOM" "D-GEOM" "P-GEOM" "Q-GEOM"
	   ;; Hypergeometric
	   "R-HYPER" "D-HYPER" "P-HYPER" "Q-HYPER"
	   ;; Logistic
	   "R-LOGIS" "D-LOGIS" "P-LOGIS" "Q-LOGIS"
	   ;; Weibull
	   "R-WEIBULL" "D-WEIBULL" "P-WEIBULL" "Q-WEIBULL"
	   ;; Wilcoxon rank sum
	   "R-WILCOX" "D-WILCOX" "P-WILCOX" "Q-WILCOX"
	   ;; Wilcoxon signed rank
	   "R-SIGNRANK" "D-SIGNRANK" "P-SIGNRANK" "Q-SIGNRANK"
	   ;; Studentized range
	   "P-TUKEY" "Q-TUKEY"
	   ))

(defpackage "RMATH.FFI"
  (:use))

(defpackage "RMATH.TEST"
  (:use "CL" "RMATH")
  (:export "RUN"))
