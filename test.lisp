(in-package :rmath.test)

(defun run ()
  (5am:run! 'rmath-suite))

(5am:def-suite rmath-suite)

(5am:in-suite rmath-suite)

#+SBCL
(5am:test sbcl-nan
  (5am:is-true (sb-ext:float-nan-p *na*)))

#-SBCL
(5am:test isnan-true-not-sbcl
  (5am:is-true (isnan (beta -2d0 -3d0))))

#+SBCL
(5am:test isnan-true-sbcl
  (5am:is-true (isnan (sb-int:with-float-traps-masked
			  (:invalid) (beta -2d0 -3d0)))))
(5am:test isnan-true-nan
  (5am:is-true (isnan *na*)))

(5am:test isnan-false
  (5am:is-true (not (isnan (beta 2d0 3d0)))))

(5am:test isnan-false-posinf
  (5am:is-true (not (isnan *posinf*))))

#+CCL
(5am:test isnan-ccl-nan
  (5am:is-true (isnan ccl::double-float-nan)))

#+CCL
(5am:test isnan-ccl-posinf
  (5am:is-true (not (isnan ccl::double-float-positive-infinity))))

#+SBCL
(5am:test isnan-sbcl-posinf
  (5am:is-true (not (isnan sb-ext:double-float-positive-infinity))))

(5am:test isfinite-true
  (5am:is-true (isfinite (beta 2d0 3d0))))

#-SBCL
(5am:test isfinite-false-nan-not-sbcl
  (5am:is-true (not (isfinite (beta -2d0 -3d0)))))

#+SBCL
(5am:test isfinite-false-nan-sbcl
  (5am:is-true (not (isfinite (sb-int:with-float-traps-masked
				  (:invalid) (beta -2d0 -3d0))))))

(5am:test isfinite-false-neginf
  (5am:is-true (not (isfinite *neginf*))))

#+CCL
(5am:test isfinite-ccl-nan
  (5am:is-true (not (isfinite ccl::double-float-nan))))

#+CCL
(5am:test isfinite-ccl-neginf
  (5am:is-true (not (isfinite ccl::double-float-negative-infinity))))

#+SBCL
(5am:test isfinite-sbcl-neginf
  (5am:is-true (not (isfinite sb-ext:double-float-negative-infinity))))

(5am:test random-seed-1
  (5am:is-true (progn
		 (setf (rng-seed) '(123 456))
		 (and (= (random-exponential) 1.2314311250480485D0)
		      (equal (rng-seed) '(4547187 8208000))
		      (member (random-exponential)
			      '(2.169415457301933D0
				;; slight difference in 32bit/64bit
				2.1694154573019335D0))
		      (equal (rng-seed) '(931729776 288000125))))))

(5am:test random-seed-2
  (5am:is-true (progn
		 (setf (rng-seed) '(123 456))
		 (and (= (r-unif 0 100) 38.45709861220258D0)
		      (equal (rng-seed) '(4547187 8208000))
		      (member (r-unif 0 100)
			      '(6.81233697263811D0
				;; slight difference in 32bit/64bit
				6.812336972638111D0))
		      (equal (rng-seed) '(931729776 288000125))))))

(5am:test normal-rng-kind-1
  (5am:is-true (progn
		 (setf (rng-seed) '(123 456)
		       (normal-rng-kind) :inversion)
		 (and (= (random-normal) -0.2934974176120066D0)
		      (eq (normal-rng-kind) :inversion)
		      ;; calls unif_rand twice to increase precision
		      (equal (rng-seed) '(931729776 288000125))))))

(5am:test normal-rng-kind-2
  (5am:is-true (progn
		 (setf (rng-seed) '(123 456)
		       (normal-rng-kind) :box-muller)
		 (let ((rn (random-normal)))
		   (when (equal (rng-seed) '(123 456))
		     ;; discard the stored value
		     (setf rn (random-normal)))
		   (and (= rn -1.734578032014886D0)
			(eq (normal-rng-kind) :box-muller)
			(equal (rng-seed) '(931729776 288000125)))))))

(5am:test normal-rng-kind-3
  (5am:is-true (progn
		 (setf (rng-seed) '(123 456)
		       (normal-rng-kind) :ahrens-dieter)
		 (and (= (random-normal) 1.119638151631605D0)
		      (eq (normal-rng-kind) :ahrens-dieter)
		      (equal (rng-seed) '(4547187 8208000))))))
