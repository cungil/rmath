;; Copyright (c) 2006-2019 Carlos Ungil

(in-package :rmath.ffi)

;; http://cran.r-project.org/doc/manuals/R-exts.html#Distribution-functions

#|6.7.1 Distribution functions

The routines used to calculate densities, cumulative distribution functions and
quantile functions for the standard statistical distributions are available as
entry points.

The arguments for the entry points follow the pattern of those for the normal
distribution:

double dnorm(double x, double mu, double sigma, int give_log);
double pnorm(double x, double mu, double sigma, int lower_tail, int give_log);
double qnorm(double p, double mu, double sigma, int lower_tail, int log_p);
double rnorm(double mu, double sigma);

That is, the first argument gives the position for the density and CDF and
probability for the quantile function, followed by the distribution’s
parameters. Argument lower_tail should be TRUE (or 1) for normal use, but can be
FALSE (or 0) if the probability of the upper tail is desired or specified.

Finally, give_log should be non-zero if the result is required on log scale, and
log_p should be non-zero if p has been specified on log scale.

Note that you directly get the cumulative (or “integrated”) hazard function,
H(t) = - log(1 - F(t)), by using

-pdist(t, ..., /*lower_tail = */ FALSE, /* give_log = */ TRUE)

or shorter (and more cryptic) -pdist(t, ..., 0, 1).

The random-variate generation routine rnorm returns one normal variate. See
Random numbers, for the protocol in using the random-variate routines.

Note that these argument sequences are (apart from the names and that rnorm has
no n) mainly the same as the corresponding R functions of the same name, so the
documentation of the R functions can be used. Note that the exponential and
gamma distributions are parametrized by scale rather than rate.

For reference, the following table gives the basic name (to be prefixed by ‘d’,
‘p’, ‘q’ or ‘r’ apart from the exceptions noted) and distribution-specific
arguments for the complete set of distributions.

    beta                        beta             a, b
    non-central beta            nbeta            a, b, ncp
    binomial                    binom            n, p
    Cauchy                      cauchy           location, scale
    chi-squared                 chisq            df
    non-central chi-squared     nchisq           df, ncp
    exponential                 exp              scale (and not rate)
    F                           f                n1, n2
    non-central F               nf               n1, n2, ncp
    gamma                       gamma            shape, scale
    geometric                   geom             p
    hypergeometric              hyper            NR, NB, n
    logistic                    logis            location, scale
    lognormal                   lnorm            logmean, logsd
    negative binomial           nbinom           size, prob
    normal                      norm             mu, sigma
    Poisson                     pois             lambda
    Student’s t                 t                n
    non-central t               nt               df, delta
    Studentized range           tukey (*)        rr, cc, df
    uniform                     unif             a, b
    Weibull                     weibull          shape, scale
    Wilcoxon rank sum           wilcox           m, n
    Wilcoxon signed rank        signrank         n

Entries marked with an asterisk only have ‘p’ and ‘q’ functions available, and
none of the non-central distributions have ‘r’ functions. After a call to
dwilcox, pwilcox or qwilcox the function wilcox_free() should be called, and
similarly for the signed rank functions.

(If remapping is suppressed, the Normal distribution names are Rf_dnorm4,
Rf_pnorm5 and Rf_qnorm5.)

For the negative binomial distribution (‘nbinom’), in addition to the
(size, prob) parametrization, the alternative (size, mu)
parametrization is provided as well by functions ‘[dpqr]nbinom_mu()’,
see ?NegBinomial in R.

Functions dpois_raw(x, *) and dbinom_raw(x, *) are versions of the Poisson and
binomial probability mass functions which work continuously in x, whereas
dbinom(x,*) and dpois(x,*) only return non zero values for integer x.

double dbinom_raw(double x, double n, double p, double q, int give_log)
double dpois_raw (double x, double lambda, int give_log)

Note that dbinom_raw() gets both p and q = 1-p which may be advantageous when
one of them is close to 1.|#


;; NOT INCLUDED IN RMATH.H - FIXME
(cffi:defcfun "wilcox_free" :void)
(cffi:defcfun "signrank_free" :void)

;; Normal Distribution
;; norm (mu,sigma)
(cffi:defcfun ("dnorm4" dnorm) :double (x :double) (mu :double) (sigma :double) (give-log :int))
(cffi:defcfun ("pnorm5" pnorm) :double (x :double) (mu :double) (sigma :double) (lower-tail :int) (give-log :int))
(cffi:defcfun ("qnorm5" qnorm) :double (p :double) (mu :double) (sigma :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rnorm" :double (mu :double) (sigma :double))
;; UNUSED
(cffi:defcfun "pnorm_both" :void (x :double) (cum :pointer) (ccum :pointer) (lower-tail :int) (log-p :int))

;; Uniform Distribution
;; unif (a,b)
(cffi:defcfun "dunif" :double (x :double) (a :double) (b :double) (give-log :int))
(cffi:defcfun "punif" :double (x :double) (a :double) (b :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qunif" :double (p :double) (a :double) (b :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "runif" :double (a :double) (b :double))

;; Gamma Distribution 
;; gamma (shape,scale)
(cffi:defcfun "dgamma" :double (x :double) (shape :double) (scale :double) (give-log :int))
(cffi:defcfun "pgamma" :double (x :double) (shape :double) (scale :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qgamma" :double (p :double) (shape :double) (scale :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rgamma" :double (shape :double) (scale :double))

;; Beta Distribution
;; beta (a,b)
(cffi:defcfun "dbeta" :double (x :double) (a :double) (b :double) (give-log :int))
(cffi:defcfun "pbeta" :double (x :double) (a :double) (b :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qbeta" :double (p :double) (a :double) (b :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rbeta" :double (a :double) (b :double))
;; UNUSED, and not defined in linux
;; (cffi:defcfun "pbeta_raw" :double (x :double) (a :double) (b :double) (lower-tail :int))

;; Lognormal Distribution
;; lnorm (logmean,logsd)
(cffi:defcfun "dlnorm" :double (x :double) (logmean :double) (logsd :double) (give-log :int))
(cffi:defcfun "plnorm" :double (x :double) (logmean :double) (logsd :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qlnorm" :double (p :double) (logmean :double) (logsd :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rlnorm" :double (logmean :double) (logsd :double))

;; Chi-squared Distribution
;; chisq (df)
(cffi:defcfun "dchisq" :double (x :double) (df :double) (give-log :int))
(cffi:defcfun "pchisq" :double (x :double) (df :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qchisq" :double (p :double) (df :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rchisq" :double (df :double))
;; UNUSED, and not defined in linux
;; (cffi:defcfun "qchisq_appr" :double (p :double) (df :double) (lgamma :double) (lower-tail :int) (log-p :int) (tol :double))

;; Non-central Chi-squared Distribution
;; nchisq (df,lambda)
(cffi:defcfun "dnchisq" :double (x :double) (df :double) (lambda :double) (give-log :int))
(cffi:defcfun "pnchisq" :double (x :double) (df :double) (lambda :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qnchisq" :double (p :double) (df :double) (lambda :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rnchisq" :double (df :double) (lambda :double))
;; I though the "r" functions were never defined for non-central distributions?

;; F Distibution
;; f (n1,n2)
(cffi:defcfun "df" :double (x :double) (n1 :double) (n2 :double) (give-log :int))
(cffi:defcfun "pf" :double (x :double) (n1 :double) (n2 :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qf" :double (p :double) (n1 :double) (n2 :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rf" :double (n1 :double) (n2 :double))

;; Student t Distibution 
;; t (n) 
(cffi:defcfun "dt" :double (x :double) (n :double) (give-log :int))
(cffi:defcfun "pt" :double (x :double) (n :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qt" :double (p :double) (n :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rt" :double (n :double))

;; Binomial Distribution
;; binom (n,p)
(cffi:defcfun "dbinom" :double (x :double) (n :double) (p :double) (give-log :int))
(cffi:defcfun "pbinom" :double (x :double) (n :double) (p :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qbinom" :double (p :double) (n :double) (pp :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rbinom" :double (n :double) (p :double))

;; Cauchy Distribution
;; cauchy (location,scale)
(cffi:defcfun "dcauchy" :double (x :double) (location :double) (scale :double) (give-log :int))
(cffi:defcfun "pcauchy" :double (x :double) (location :double) (scale :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qcauchy" :double (p :double) (location :double) (scale :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rcauchy" :double (location :double) (scale :double))

;; Exponential Distribution 
;; exp (scale)
(cffi:defcfun "dexp" :double (x :double) (scale :double) (give-log :int))
(cffi:defcfun "pexp" :double (x :double) (scale :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qexp" :double (p :double) (scale :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rexp" :double (scale :double))

;; Geometric Distribution
;; geom (p) 
(cffi:defcfun "dgeom" :double (x :double) (p :double) (give-log :int))
(cffi:defcfun "pgeom" :double (x :double) (p :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qgeom" :double (p :double) (pp :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rgeom" :double (p :double))

;; Hypergeometric Distibution 
;; hyper (NR,NB,n)
(cffi:defcfun "dhyper" :double (x :double) (nr :double) (nb :double) (n :double) (give-log :int))
(cffi:defcfun "phyper" :double (x :double) (nr :double) (nb :double) (n :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qhyper" :double (p :double) (nr :double) (nb :double) (n :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rhyper" :double (nr :double) (nb :double) (n :double))

;; Negative Binomial Distribution
;; nbinom (n,p)
(cffi:defcfun "dnbinom" :double (x :double) (n :double) (p :double) (give-log :int))
(cffi:defcfun "pnbinom" :double (x :double) (n :double) (p :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qnbinom" :double (p :double) (n :double) (pp :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rnbinom" :double (n :double) (p :double))

;; Poisson Distribution
;; pois (lambda)
(cffi:defcfun "dpois" :double (x :double) (lambda :double) (give-log :int))
(cffi:defcfun "ppois" :double (x :double) (lambda :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qpois" :double (p :double) (lambda :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rpois" :double (lambdax :double))

;; Weibull Distribution
;; weibull (shape,scale)
(cffi:defcfun "dweibull" :double (x :double) (shape :double) (scale :double) (give-log :int))
(cffi:defcfun "pweibull" :double (x :double) (shape :double) (scale :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qweibull" :double (p :double) (shape :double) (scale :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rweibull" :double (shape :double) (scale :double))

;; Logistic Distribution
;; logis (location,scale)
(cffi:defcfun "dlogis" :double (x :double) (location :double) (scale :double) (give-log :int))
(cffi:defcfun "plogis" :double (x :double) (location :double) (scale :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qlogis" :double (p :double) (location :double) (scale :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rlogis" :double (location :double) (scale :double))

;; Wilcoxon Rank Sum Distribution 
;; wilcox (m,n)
(cffi:defcfun "dwilcox" :double (x :double) (m :double) (n :double) (give-log :int))
(cffi:defcfun "pwilcox" :double (x :double) (m :double) (n :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qwilcox" :double (p :double) (m :double) (n :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rwilcox" :double (m :double) (n :double))

;; Wilcoxon Signed Rank Distribution
;; signrank (n)
(cffi:defcfun "dsignrank" :double (x :double) (n :double) (give-log :int))
(cffi:defcfun "psignrank" :double (x :double) (n :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qsignrank" :double (p :double) (n :double) (lower-tail :int) (log-p :int))
(cffi:defcfun "rsignrank" :double (n :double))

;;;
;;; incomplete (not all four functions defined)
;;;

;; Non-central Beta Distribution
;; nbeta (a,b,lambda)
(cffi:defcfun "dnbeta" :double (x :double) (a :double) (b :double) (lambda :double) (give-log :int))
(cffi:defcfun "pnbeta" :double (x :double) (a :double) (b :double) (lambda :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qnbeta" :double (p :double) (a :double) (b :double) (lambda :double) (lower-tail :int) (log-p :int))
;; UNDEFINED
;; (cffi:defcfun "rnbeta" :double (a :double) (b :double) (lambda :double))

;; Non-central F Distribution
;; nf (n1,n2,ncp) 
(cffi:defcfun "dnf" :double (x :double) (n1 :double) (n2 :double) (ncp :double) (give-log :int))
(cffi:defcfun "pnf" :double (x :double) (n1 :double) (n2 :double) (ncp :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qnf" :double (p :double) (n1 :double) (n2 :double) (ncp :double) (lower-tail :int) (log-p :int))

;; Non-central Student t Distribution
;; nt (df,delta)
(cffi:defcfun "dnt" :double (x :double) (df :double) (delta :double) (give-log :int))
(cffi:defcfun "pnt" :double (x :double) (df :double) (delta :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qnt" :double (p :double) (df :double) (delta :double) (lower-tail :int) (log-p :int))

;; Studentized Range Distribution 
;; tukey (rr,cc,df)
(cffi:defcfun "ptukey" :double (x :double) (rr :double) (cc :double) (df :double) (lower-tail :int) (give-log :int))
(cffi:defcfun "qtukey" :double (p :double) (rr :double) (cc :double) (df :double) (lower-tail :int) (log-p :int))

;; Multinomial Distribution 

;; void rmultinom(int, double*, int, int*);
